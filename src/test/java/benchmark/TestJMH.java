package benchmark;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.annotationBasedBeanDefinitions.Order;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
public class TestJMH {
    private Map<String, Order> map = new HashMap<>();
    private ClassPathXmlApplicationContext context;

    @Setup(Level.Trial)
    public void setup() {
        map.put("bean1", new Order());
        context = new ClassPathXmlApplicationContext("annotationBasedBeanDefinitions.xml");
    }

    @Benchmark
    public void getFromMap() {
        Order order = (Order)map.get("bean1");
    }

    @Benchmark
    public void getFromContext() {
        Order order = (Order)context.getBean(Order.class);
    }

    @Benchmark
    public void getFromContextByName() {
        Order order = (Order)context.getBean("order");
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                //.include(TestJMH.class.getSimpleName())
                .mode(Mode.AverageTime)
                .timeUnit(TimeUnit.NANOSECONDS)
                .warmupIterations(1)
                .measurementIterations(1)
                .forks(1).build();

        new Runner(options).run();
    }
}
