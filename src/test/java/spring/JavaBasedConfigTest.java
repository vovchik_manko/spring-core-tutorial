package spring;

import spring.annotationBasedBeanDefinitions.Order;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JavaBasedConfigTest {
    @Test
    public void testJavaBasedConfig() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("annotationBasedBeanDefinitions.xml");

        Order order = (Order)context.getBean(Order.class);
        System.out.println(order);

        context.close();
    }
}
