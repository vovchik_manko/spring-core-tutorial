package spring.xmlBeanDefinitions;

import spring.xmlBeanDefinitions.testClasses.Order;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutowireTest {
    @Test
    public void testOrderWithAutowireByName() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/autowire/orderWithAutowireByName.xml");

        // NOTE: property name and bean name should be the same
        Order order = (Order) context.getBean("orderWithAutowireByName");
        System.out.println(order.toString());
    }

    @Test
    public void testOrderWithAutowireByType() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/autowire/orderWithAutowireByType.xml");

        Order order = (Order) context.getBean("orderWithAutowireByType");
        System.out.println(order.toString());
    }

    @Test
    public void testOrderWithAutowireByConstructor() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/autowire/orderWithAutowireByConstructor.xml");

        Order order = (Order) context.getBean("orderWithAutowireByConstructor");
        System.out.println(order.toString());
    }
}
