package spring;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.postProcessor.Quoter;

import java.util.Arrays;

public class TerminatorQuoterTest {
    @Test
    public void sayQuote() throws Exception {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("postProcessor/contextPostProcessor.xml");
        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
        context.getBean(Quoter.class).sayQuote();
    }

}