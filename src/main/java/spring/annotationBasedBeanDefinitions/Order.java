package spring.annotationBasedBeanDefinitions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Order {
    @Autowired
    private Person  sender;

    @Autowired
    @Qualifier(value = "anotherPerson")
    private Person  recipient;
    private Integer amount;

    public Person getSender() {
        return sender;
    }

    @Required
    public void setSender(Person sender) {
        this.sender = sender;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Order is constructed");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Order will be destroyed soon");
    }

    @Override
    public String toString() {
        return "Order [sender=" + sender + ", recipient=" + recipient + ", amount = " + amount + "]";
    }
}
