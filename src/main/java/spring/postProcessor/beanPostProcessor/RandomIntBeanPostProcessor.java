package spring.postProcessor.beanPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.ReflectionUtils;
import spring.postProcessor.annotation.InjectRandomInt;

import java.lang.reflect.Field;
import java.util.Random;

public class RandomIntBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String name) throws BeansException {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            InjectRandomInt randomIntAnnotation = field.getAnnotation(InjectRandomInt.class);
            if (null != randomIntAnnotation) {
                int min = randomIntAnnotation.min();
                int max = randomIntAnnotation.max();

                Random random = new Random();
                int randomValue = min + random.nextInt(max - min);
                field.setAccessible(true);
                ReflectionUtils.setField(field, bean, randomValue);
            }
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String name) throws BeansException {
        return bean;
    }
}
