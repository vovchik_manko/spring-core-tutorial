package spring.postProcessor;

import spring.postProcessor.annotation.InjectRandomInt;
import spring.postProcessor.annotation.Transactional;

@Transactional
public class TerminatorQuoter implements Quoter {
    @InjectRandomInt(min=1, max=7)
    private int repeat = 1;

    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void sayQuote() {
        for (int i = 0; i < repeat; i++) {
            System.out.println("message = " + message);
        }
    }
}
