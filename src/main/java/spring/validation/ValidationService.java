package spring.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;

import javax.validation.Valid;
import java.util.Date;

@Service
public class ValidationService {
    @Autowired
    private Validator validator;

    public BindingResult checkCustomer(@Valid Customer customer) {
        DataBinder binder = new DataBinder(customer);
        binder.setValidator(validator);
        binder.validate();

        return binder.getBindingResult();
    }
}
