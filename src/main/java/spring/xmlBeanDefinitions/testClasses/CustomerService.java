package spring.xmlBeanDefinitions.testClasses;

public class CustomerService {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
