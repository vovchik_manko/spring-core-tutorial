package spring.xmlBeanDefinitions.testClasses;

public class Order {
    private Person person;
    private Integer amount;

    public Order() {

    }

    public Order(Person person, Integer amount) {
        this.person = person;
        this.amount = amount;

    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Order [person=" + person + ", amount = " + amount + "]";
    }
}
